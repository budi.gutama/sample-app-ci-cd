import os
from flask import Flask
from handlers.routes import configure_routes

app = Flask(__name__)

configure_routes(app)

if __name__ == '__main__':
    app.run(
        os.getenv('HOST', '0.0.0.0'), 
        os.getenv('PORT', '3000'),
        os.getenv('DEBUG', False)
        )